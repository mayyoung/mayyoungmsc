# README #

By May Young

Compile rangedCoherenceTool:
g++ -std=c++11 -o rangedCoherenceTool rangedCoherenceTool.cpp

Run ./rangedCoherenceTool with "-h" for more help options.

Example command to analyze a file called simple.ll for races:
./rangedCoherenceTool -in simple.ll -cache-line 64 -wb 64

How to analyze a C file for races:
1. Manually instrument the C file (e.g., fake.c) to output the loads/stores.

2. Execute the C file: ./fake > preout.txt

3. In preout.txt, manually type in "TRACESTART" immediately before the first line of the first operation, and "TRACEEND" immediately after the last line to be analyzed.

4. Filter out irrelevant output and keep all the operations together using processTrace.py into a new file called out.txt. With the fake.c example, there would be no difference between preout.txt and out.txt. 
python3 ./processTrace.py

5. Run step2processTrace.py to consolidate similar operations together, taking out.txt as input and outputting final.txt.
python3 ./step2processTrace.py

6. Pass final.txt as the input file (optionally, rename it to a .ll file extension) to rangedCoherenceTool.
./rangedCoherenceTool -in final.txt -cache-line 64 -wb 64