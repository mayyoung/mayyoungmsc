import sys
import os
import re

def getOp(strLine):
    return strLine.split(" ")[0]

def getLow(strLine):
    return strLine.split("-")[0].split(" ")[1]

def getHigh(strLine):
    return strLine.split("-")[1].rstrip()

def getPrintLine(myList):
    # return "<op> <low>-<high>\n"
    return myList[0] + " " + myList[3] + "-" + myList[4] + "\n"

def updateList(li, line, of):
    if len(li) == 0:
        li.append(getOp(line))
        li.append(getLow(line))
        li.append(getHigh(line))
        li.append(getLow(line))
        li.append(getHigh(line))
    else:
        # if prev's high + 1 == this line's low, they're contiguous
        if int(li[2], 0) + 1 == int(getLow(line), 0):
            li[4] = getHigh(line)
        else:
            of.write(getPrintLine(li))
            li[3] = getLow(line)
            li[4] = getHigh(line)
        li[1] = getLow(line)
        li[2] = getHigh(line)

def main():
    start = False
    cached = False
    uncached = False
    cr = []
    cw = []
    ur = []
    uw = []
    dr = []
    dw = []
    with open("out.txt", "r") as f:
        with open("final.txt", "w+") as of:
            for line in f:
                # [ 0  ,     1     ,      2     ,       3    ,      4     ] 
                # ["op", "low_addr", "high_addr", "first_low", "last_high"]
                
                # We cannot use the 'find' function because 'cached_read'
                # is contained in 'uncached_read'
                if re.search(r'\bcached_read\b', line):
                    if len(cw) > 0:
                        of.write(getPrintLine(cw))
                        cw = []
                    if len(ur) > 0:
                        of.write(getPrintLine(ur))
                        ur = []
                    if len(uw) > 0:
                        of.write(getPrintLine(uw))
                        uw = []
                    if len(dr) > 0:
                        of.write(getPrintLine(dr))
                        dr = []
                    if len(dw) > 0:
                        of.write(getPrintLine(dw))
                        dw = []
                    updateList(cr, line, of) 
                elif re.search(r'\bcached_write\b', line):
                    if len(cr) > 0:
                        of.write(getPrintLine(cr))
                        cr = []
                    if len(ur) > 0:
                        of.write(getPrintLine(ur))
                        ur = []
                    if len(uw) > 0:
                        of.write(getPrintLine(uw))
                        uw = []
                    if len(dr) > 0:
                        of.write(getPrintLine(dr))
                        dr = []
                    if len(dw) > 0:
                        of.write(getPrintLine(dw))
                        dw = []
                    updateList(cw, line, of)
                elif re.search(r'\buncached_read\b', line): 
                    if len(cr) > 0:
                        of.write(getPrintLine(cr))
                        cr = []
                    if len(cw) > 0:
                        of.write(getPrintLine(cw))
                        cw = []
                    if len(uw) > 0:
                        of.write(getPrintLine(uw))
                        uw = []
                    if len(dr) > 0:
                        of.write(getPrintLine(dr))
                        dr = []
                    if len(dw) > 0:
                        of.write(getPrintLine(dw))
                        dw = []
                    updateList(ur, line, of)
                elif re.search(r'\buncached_write\b', line): 
                    if len(cr) > 0:
                        of.write(getPrintLine(cr))
                        cr = []
                    if len(cw) > 0:
                        of.write(getPrintLine(cw))
                        cw = []
                    if len(ur) > 0:
                        of.write(getPrintLine(ur))
                        ur = []
                    if len(dr) > 0:
                        of.write(getPrintLine(dr))
                        dr = []
                    if len(dw) > 0:
                        of.write(getPrintLine(dw))
                        dw = []
                    updateList(uw, line, of)
                elif re.search(r'\bdo_dma_read\b', line):
                    if len(cr) > 0:
                        of.write(getPrintLine(cr))
                        cr = []
                    if len(cw) > 0:
                        of.write(getPrintLine(cw))
                        cw = []
                    if len(ur) > 0:
                        of.write(getPrintLine(ur))
                        ur = []
                    if len(uw) > 0:
                        of.write(getPrintLine(uw))
                        uw = []
                    if len(dw) > 0:
                        of.write(getPrintLine(dw))
                        dw = []
                    updateList(dr, line, of) 
                elif re.search(r'\bdo_dma_write\b', line):
                    if len(cr) > 0:
                        of.write(getPrintLine(cr))
                        cr = []
                    if len(cw) > 0:
                        of.write(getPrintLine(cw))
                        cw = []
                    if len(ur) > 0:
                        of.write(getPrintLine(ur))
                        ur = []
                    if len(uw) > 0:
                        of.write(getPrintLine(uw))
                        uw = []
                    if len(dr) > 0:
                        of.write(getPrintLine(dr))
                        dr = []
                    updateList(dw, line, of) 
                elif re.search(r'\bsync\b', line):
                    if len(cr) > 0:
                        of.write(getPrintLine(cr))
                        cr = []
                    if len(cw) > 0:
                        of.write(getPrintLine(cw))
                        cw = []
                    if len(ur) > 0:
                        of.write(getPrintLine(ur))
                        ur = []
                    if len(uw) > 0:
                        of.write(getPrintLine(uw))
                        uw = []
                    if len(dr) > 0:
                        of.write(getPrintLine(dr))
                        dr = []
                    if len(dw) > 0:
                        of.write(getPrintLine(dw))
                        dw = []
                    of.write(line)
                elif re.search(r'\bcache_flusha\b', line):
                    if len(cr) > 0:
                        of.write(getPrintLine(cr))
                        cr = []
                    if len(cw) > 0:
                        of.write(getPrintLine(cw))
                        cw = []
                    if len(ur) > 0:
                        of.write(getPrintLine(ur))
                        ur = []
                    if len(uw) > 0:
                        of.write(getPrintLine(uw))
                        uw = []
                    if len(dr) > 0:
                        of.write(getPrintLine(dr))
                        dr = []
                    if len(dw) > 0:
                        of.write(getPrintLine(dw))
                        dw = []
                    of.write(line)
                
            # Write last line of file
            if len(cr) > 0:
                of.write(getPrintLine(cr))
                cr = []
            if len(cw) > 0:
                of.write(getPrintLine(cw))
                cw = []
            if len(ur) > 0:
                of.write(getPrintLine(ur))
                ur = []
            if len(uw) > 0: 
                of.write(getPrintLine(uw))
                uw = []
            if len(dr) > 0:
                of.write(getPrintLine(dr))
                dr = []
            if len(dw) > 0:
                of.write(getPrintLine(dw))
                dw = []


if __name__ == '__main__':
    main()
