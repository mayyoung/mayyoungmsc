import sys
import os
import re

def main():
    start = False
    cached = False
    uncached = False
    with open("preout.txt", "r") as f:
        with open("out.txt", "w+") as of:
            for line in f:
                if "TRACESTART" in line:
                    start = True
                if "TRACEEND" in line:
                    break
                if not start:
                    continue
                
                # We cannot use the 'find' function because 'cached_read'
                # is contained in 'uncached_read'
                if re.search(r'\bcached_read\b', line):
                    of.write(line)
                    continue
                elif re.search(r'\bcached_write\b', line):
                    of.write(line)
                    continue
                elif re.search(r'\bcached\b', line): 
                    uncached = False
                    cached = True
                    continue
                elif re.search(r'\buncached\b', line): 
                    cached = False
                    uncached = True
                    continue
                elif re.search(r'\bdo_dma_read\b', line):
                    of.write(line)
                    continue
                elif re.search(r'\bdo_dma_write\b', line):
                    of.write(line)
                    continue
                elif re.search(r'\bsync\b', line):
                    of.write(line)
                    continue
                elif not line.startswith("_", 0, 1):
                    continue

                if cached:
                    cachedLine = "cached" + line
                    of.write(cachedLine)
                if uncached:
                    uncachedLine = "uncached" + line
                    of.write(uncachedLine)

if __name__ == '__main__':
    main()
