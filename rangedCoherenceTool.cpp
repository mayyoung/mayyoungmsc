// Updated September 1, 2019
#include <fstream>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <stack>
#include <algorithm> // std::find, std::max, std::min
#include <iomanip>   // std::setfill, std::setw

// Working with C-strings
#include <string.h> // strcmp
#include <stdlib.h> // atoi

using namespace std;

//#define DEBUG // Uncomment for DEBUG mode with prints
                // OR add -DDEBUG flag to g++ command

#ifdef DEBUG
#define DEBUG_STDOUT(x) (cout << (x) << endl)
#else
#define DEBUG_STDOUT(x)
#endif

typedef enum NodeType {CPU, CACHE, ACC} nodeType;
bool ignoreCpuCpuRaces;

// Source: https://stackoverflow.com/questions/2202262/
class Range
{
public:
    Range() {}
    
    explicit Range(long addr) // [addr,addr]
    {
        mLow = addr;
        mHigh = addr;
    }
    
    Range(long low, long high) // [low,high]
    {
        mLow = low;
        mHigh = high;
    }

    bool operator==(const Range& rhs) const
    {
        return mLow == rhs.mLow && mHigh == rhs.mHigh;
    }

    bool operator<(const Range& rhs) const
    {
        if (mLow < rhs.mLow)
            return true;
        return false;
    }

    void setLow(long newLow)
    {
        mLow = newLow;
    }

    void setHigh(long newHigh)
    {
        mHigh = newHigh;
    }

    /* Return true if rhs range overlaps with this range,
       including boundaries and if rhs is fully contained 
       within this range. False otherwise.
    */
    bool overlaps(Range const& rhs) const
    {
        return mLow <= rhs.mHigh && rhs.mLow <= mHigh;
    }

    /* Return the intersection of this range and rhs' range.
       If no intersection, return nullptr.
       Recall: call "delete" on the returned pointer.
    */
    Range* getIntersection(Range rhs)
    {
        Range* r = new Range(std::max(mLow, rhs.mLow),
                             std::max(mHigh, rhs.mHigh));
        if (r->high() < r->low())
            return nullptr;
        return r;
    }

    long low() const { return mLow; }
    long high() const { return mHigh; }

private:
    long mLow;
    long mHigh;
}; // class Range

class Node
{
public:

    int index;
    string name;
    Range addrRange;
    NodeType nodeType;
    vector<Node*> neighbours;

    // Constructor
    Node(NodeType _nodeType, int _index, string _name, Range _addrRange) 
    {
        nodeType = _nodeType;
        index = _index;
        name = _name;
        addrRange = _addrRange;
    }

    bool operator==(const Node& rhs) const
    {
        return (this->name == rhs.name && this->addrRange.low() == rhs.addrRange.low() &&
                this->addrRange.high() == rhs.addrRange.high() &&
                this->nodeType == rhs.nodeType);
    }
}; // class Node

bool isRelevantOperation(string op) 
{
    if (op.find("cached_read") != string::npos || 
        op.find("cached_write") != string::npos ||
        op.find("cache_flusha") != string::npos || 
        op.find("do_dma_read") != string::npos ||
        op.find("do_dma_write") != string::npos ||
        op.find("sync") != string::npos ||
        op.find("uncached_read") != string::npos ||
        op.find("uncached_write") != string::npos)
    {
        return true;
    }
    return false;
}

void updateGraph(map< Node*, vector<Node*> >* graph, Node* key, vector<Node*> value)
{
#ifdef DEBUG
    auto res = graph->emplace(key, value); 
    // If the key already exists, update the value only
    if (!res.second)
    {
        DEBUG_STDOUT("Graph emplace did not succeed, update key " + key->name);
        res.first->second = value;
    }
#endif
}

/* 
Perform DFS to check if there is a path or not between src and dest.
Return true if there is a path, false otherwise.
*/
bool existsPath(Node* src, Node* dest, vector<bool> &visited)
{
    // Create a stack ofr iterative DFS
    stack<Node*> stack;

    stack.push(src);

    while (!stack.empty())
    {
        src = stack.top();
        stack.pop();

        if (!visited[src->index])
            visited[src->index] = true;

        // Get all neighbours of src. If a neighbour has not been visited
        // before, push it onto the stack
        for (auto u : src->neighbours)
        {
            if (*u == *dest)
                return true;

            if (!visited[u->index])
                stack.push(u);
        }
    }

    return false;
}

/* 
Check if there exists a path between two nodes that access the same
location/address in shared memory, in BOTH directions.
Note: Lesser node means it happens earlier. 
      Greater node means it happens later in the graph.
The greater node is the current node being created.
Return true if there is a race detected or if at least one of the given
nodes is NULL, false otherwise.
*/
bool isRace(Node* lesserNode, Node* greaterNode, vector<bool> &visited)
{
    if (!lesserNode || !greaterNode)
        return true;

    for (int i = 0; i < visited.size(); i++)
        visited[i] = false;

    // Check if there is a path from lesserNode to greaterNode
    bool hasPath = existsPath(lesserNode, greaterNode, visited);

    // If we did not find a path from lesserNode to greaterNode,
    // check if there is a path from greaterNode to lesserNode
    if (!hasPath)
    {
        hasPath = existsPath(greaterNode, lesserNode, visited);
    }
    

    return (hasPath ? false : true);
}

void printRaceMessage(string name1, string name2)
{
    cout << "!! RACE between " << name1 << " and " << name2 << " !!" << endl;
}

string longToHex(long l)
{
    stringstream stream;
    stream << "0x" << setfill('0') << std::hex << l;
    return stream.str();
}

void printRaceMessage2(Node* node1, Node* node2)
{
    string name1 = node1->name;
    string name2 = node2->name;
    cout << "!! RACE between " << name1 << " and " << name2 << " !!" << endl;
    cout << name1 << " addr: " << longToHex(node1->addrRange.low()) << "-"
                               << longToHex(node1->addrRange.high()) << endl;
    cout << name2 << " addr: " << longToHex(node2->addrRange.low()) << "-"
                               << longToHex(node2->addrRange.high()) << endl;
}

// Source: https://stackoverflow.com/questions/5100718/
string intToHex(int i)
{
    stringstream stream;
    stream << "0x" 
        << setfill ('0') //<< setw(sizeof(int)*2) 
        << std::hex << i;
    return stream.str();
}

/* Return true if it's a CPU/CPU race, fale otherwise */
bool isCpuCpuRace(string first, string second)
{
    if ((first.find("wb") != string::npos &&
         second.find("uncached_write") != string::npos) ||
        (first.find("wb") != string::npos &&
         second.find("uncached_read") != string::npos))
    {
        return true;
    }

    return false;
}

bool isReadRead(string first, string second)
{
    if ((first.find("dma_r") != string::npos &&
         second.find("uncached_read") != string::npos) ||
        (first.find("uncached_read") != string::npos &&
         second.find("dma_r") != string::npos))
    {
        return true;
    }
    return false;
}

bool doRaceDetection(map<Range, Node*>* m, Node* node, int numNodes)
{
    // Stores whether node is visited or not for DFS
    vector<bool> visited(numNodes);
    for (auto it = m->begin(); it != m->end(); ++it) 
    {
        // Iterate through pairs <addrRange, Node*>
        if (node->addrRange.overlaps(it->first)) 
        {
            DEBUG_STDOUT("Prev node (" + it->second->name + " " + intToHex(it->first.low()) 
                    + " - " + intToHex(it->first.high()) + ") overlaps with " 
                    + node->name + " " + intToHex(node->addrRange.low()) + " - "
                    + intToHex(node->addrRange.high()));

            if (isRace(it->second, node, visited))
            {
                if (ignoreCpuCpuRaces && isCpuCpuRace(it->second->name, node->name))
                {
                    //printRaceMessage(it->second->name, node->name);
                    DEBUG_STDOUT("Ignore CPU/CPU self-consistency issues...");
                    return false;
                }
                else if (isReadRead(it->second->name, node->name))
                {
                    // Because lastDmaOp can store either a DMA read or write
                    // as the last op, if the last DMA op is a read and the
                    // node  we're comparing against is also a read, this is 
                    // NOT a race.
                    return false;
                }
                else
                {
                    //printRaceMessage(it->second->name, node->name);
                    printRaceMessage2(it->second, node);
                }
                return true; // Stop at first race detected
                //return false; // TODO: delete this line!!!! and uncomment above line
            }
            else { DEBUG_STDOUT("No race here! :)"); }
        }
    }
    return false;
}

void updateDmaMapsAndGraph(map<Range, Node*>* dmaMap, map<Range, Node*>* lastDmaOpMap, 
               Range range, Node* dmaNode, map< Node*, vector<Node*> >* graph)
{
    auto res = dmaMap->emplace(range, dmaNode);
    // If there is a previous relevant DMA node (read or write),
    // connect that to this newly created DMA node
    if (!res.second)
    {
        // res.first is an iterator to pair <addr, pointer_to_latest_alloc_node>
        res.first->second->neighbours.push_back(dmaNode);
        DEBUG_STDOUT("Prev dma: " + res.first->second->name);
        updateGraph(graph, res.first->second, res.first->second->neighbours);

        // The latest DMA node for this addr is now this DMA node
        res.first->second = dmaNode;
    }

    auto res2 = lastDmaOpMap->emplace(range, dmaNode);
    // Update the last DMA op to this one for addr
    if (!res2.second)
    {
        res2.first->second = dmaNode;
    }
}

/* Return true if n is power of 2, false otherwise. */
bool isPowerOf2(int n)
{
    if (n && !(n & (n-1)))
        return true;
    return false;
}

// Source: https://www.geeksforgeeks.org/smallest-power-of-2-greater-than-or-equal-to-n/
unsigned int nextPowerOf2(unsigned int n)
{
    unsigned int p = 1;
    // Return n if it's a power of 2
    if (isPowerOf2(n))
        return n;

    while (p < n)
        p <<= 1;

    return p;
}

/* Return the lower limit of the bloated address range, which is 
   the largest address below the given addr that is a multiple 
   of bloatSize.
   E.g., getLowBloatedAddress(100, 128) returns 0
         getLowBloatedAddress(128, 128) returns 128
         getLowBloatedAddress(3000, 128) returns 2944
   Ref: https://stackoverflow.com/questions/3407012
 */
long getLowBloatedAddress(long addr, int bloatSize)
{
    if (bloatSize == 0)
        return addr;

    long remainder = addr % bloatSize;
    // Return addr as is if it's already a multiple of bloatSize
    if (remainder == 0)
        return addr;

    if (addr - bloatSize < 0)
        return 0;

    return (addr - remainder);
}

/* Return the upper limit of the bloated address range, which is
   the next multiple of bloatSize of the given addr minus one.
   E.g., getHighBloatedAddress(10, 128) returns 127
         getHighBloatedAddress(256, 128) returns 255
         getHighBloatedAddress(3007, 128) returns 3071
   Ref: https://stackoverflow.com/questions/3407012
*/
long getHighBloatedAddress(long addr, int bloatSize, 
                           bool isSplit = false, bool isSplitNode2 = false)
{
    if (bloatSize == 0)
        return addr;

    long remainder = addr % bloatSize;
    // Return addr - 1, if it's already a multiple of bloatSize.
    // If it's the second split node and it's already a multiple of bloatSize,
    // return the next multiple - 1 (skip this check).
    if (remainder == 0 && isSplit && !isSplitNode2)
        return addr - 1;
    
    // - 1 because the lower part of the address needs to be cache aligned, 
    // and the number of addresses between low and high is a multiple of bloatSize,
    // i.e., high - low + 1 == bloatSize
    return (addr + bloatSize - remainder) - 1;
}

int main(int argc, char** argv)
{
    string inputFileName = "testprog.ll"; // Default
    ignoreCpuCpuRaces = false;
    int cacheLineSize = 256;
    int wbGranularity = -1;
    for (int i = 1; i < argc; i++) 
    {
        if (strcmp(argv[i], "-h") == 0)
        {
            cout << "Usage: " << argv[0] << " [options]" << endl;
            cout << "Options:" << endl;
            cout << "-h             Print this help message." << endl;
            cout << "-in            Specify input file name." << endl;
            cout << "-cache-line    Specify cache line size in bytes. Must be a power of 2. Set to 256 as default." << endl;
            cout << "-wb            Specify writeback granularity in bytes. Must be a power of 2. Set to be the same as the cache line size as default." << endl;
            cout << "-icc           Ignore CPU/CPU races." << endl;
        }
        else if (strcmp(argv[i], "-icc") == 0)
        {
            cout << "Will ignore CPU/CPU races..." << endl;
            ignoreCpuCpuRaces = true;
        }
        // For options with an arg after the option
        else if (i + 1 < argc)
        {
            if (strcmp(argv[i], "-in") == 0)
            {
                inputFileName = argv[i + 1];
            }
            else if (strcmp(argv[i], "-cache-line") == 0)
            {
                cacheLineSize = atoi(argv[i + 1]);
                if (!isPowerOf2(cacheLineSize))
                {
                    cout << "Cache line size must be a power of 2." << endl;
                    return -1;
                }
            }
            else if (strcmp(argv[i], "-wb") == 0)
            {
                wbGranularity = atoi(argv[i + 1]);
                if (!isPowerOf2(wbGranularity))
                {
                    cout << "Writeback granularity must be a power of 2." << endl;
                    return -1;
                }
            }
        }
    }

    // Set writeback granularity to be equal to cache line size, if unspecified
    if (wbGranularity == -1)
        wbGranularity = cacheLineSize;
    
    Node* head = NULL;
    Node* prev = NULL;
    int numNodes = 0;

    map< Node*, vector<Node*> > graph;
    
    map<Range, Node*> allocMap; // floating allocs
    map<Range, Node*> wbMap;    // dangling writebacks (part of active frontier)
    map<Range, Node*> dmaMap;   // DMA operations to the same address 
                                 // need to be connected together
    map<Range, Node*> lastDmaOpMap; // last DMA op for an address (part of active frontier)
    vector<Range> dmaRangesToSync; 

    // Keep track of memory to delete at the end //TODO
    vector<Node*> tracker;

    // Open input file in read mode
    ifstream inputFile;
    string line;
    inputFile.open(inputFileName);
    if (!inputFile) 
    {
        cout << "Error in opening input file" << endl;
        return -1;
    }

    DEBUG_STDOUT("Reading from input file...");

    int count = 0;
    string op = "";
    string addr = "";
    //int iStartAddr; 
    long iStartAddr; 
    //int iEndAddr;
    long iEndAddr;

    Node* newNode = NULL;
    Node* newCrNode = NULL;
    Node* newAllocNode1 = NULL;
    Node* newAllocNode2 = NULL;
    Node* newCwNode = NULL;
    Node* newWbNode1 = NULL;
    Node* newWbNode2 = NULL;
    Node* newDmarNode = NULL;
    Node* newDmawNode = NULL;
    Node* newSyncNode = NULL;

    bool hasRace = false;
    Node* connectToCpuNode = NULL;
    bool needToConnectToCpu = false;
    Node* firstCachedOpAfterFlushNode = NULL;
    bool isFirstCachedOpAfterFlush = false;
    Node* lastFlushNode = NULL;

    while (getline(inputFile, line))
    {
        // Check if we've reached EOF. Otherwise, the last line
        // of the input file would be parsed twice.
        //if (inputFile.eof()) break;

        DEBUG_STDOUT(line);
        hasRace = false;        // reset hasRace detection variable
        newWbNode2 = NULL;      // reset since we might not pre-split every time
        newAllocNode2 = NULL;   // reset since we might no pre-split every time
        
        // Assume a line consists of: <op> <address>-<address>
        // Example: cached_read 0x1000-0x1007
        size_t spacePosition = line.find(" ");
        size_t dashPosition = line.find_last_of("-");
        op = line.substr(0, spacePosition); // get op from line
        if (!isRelevantOperation(op))
            continue;
        if (spacePosition < line.length())
        {
            if (dashPosition < line.length())
            {
                // Get substring from first " " to "-" to get start address
                addr = line.substr(spacePosition, abs(dashPosition - spacePosition));
            }
            else
            {
                // Get substring from first " " to end
                addr = line.substr(spacePosition);
            }
            // Trim whitespace from address
            addr.erase(std::remove(addr.begin(), addr.end(), ' '), addr.end());
            //DEBUG_STDOUT("1 start addr stoi: " + addr);
            //iStartAddr = stoi(addr, nullptr, 0);
            iStartAddr = stol(addr, nullptr, 0);
        }
        if (dashPosition < line.length())
        {
            // Get end address from one char after "-" to the end
            addr = line.substr(dashPosition + 1);
            // Trim whitespace
            addr.erase(std::remove(addr.begin(), addr.end(), ' '), addr.end());
            //DEBUG_STDOUT("2 end addr stoi: " + addr);
            //iEndAddr = stoi(addr, nullptr, 0);
            iEndAddr = stol(addr, nullptr, 0);
        }
        else { iEndAddr = iStartAddr; }

        Range range = Range(iStartAddr, iEndAddr);

        newNode = new Node(CPU, ++numNodes, op + to_string(count), range);
        tracker.emplace_back(newNode);
        DEBUG_STDOUT("Adding " + op + to_string(count));

        // Assume no previous nodes before first line in input file.
        if (count == 0) 
        {
            head = newNode;
            prev = head;
            if (prev->name.find("do_dma_read") != string::npos ||
                prev->name.find("do_dma_write") != string::npos)
            {
                dmaRangesToSync.push_back(Range(prev->addrRange.low(), prev->addrRange.high()));
            }
        }
        else
        {
            prev->neighbours.push_back(newNode);
            updateGraph(&graph, prev, prev->neighbours);
            if (prev->name.find("do_dma_read") != string::npos ||
                prev->name.find("do_dma_write") != string::npos)
            {
                dmaRangesToSync.push_back(Range(prev->addrRange.low(), prev->addrRange.high()));
            }
            prev = newNode;
        }

        if (needToConnectToCpu)
        {
            DEBUG_STDOUT("connectToCpuNode: " + connectToCpuNode->name);
            connectToCpuNode->neighbours.push_back(newNode);
            updateGraph(&graph, connectToCpuNode, connectToCpuNode->neighbours);
            // Reset
            connectToCpuNode = NULL;
            needToConnectToCpu = false;
        }

        // We must put uncached_read and uncached_write before cached_read and
        // cached_write because, otherwise, uncached_x would be read as cached_x
        // since cached_x is contained within uncached_x.
        if (op.find("uncached_read") != string::npos)
        {
            // The node for this uncached read done by CPU 
            // is already added with newNode
            
            // Stores whether node is visited or not for DFS
            vector<bool> visited(numNodes);

            // Race detection with dangling writebacks
            // Example: uncached_read after a cached write has a race with WB
            //          without a flush in between
            if (hasRace = doRaceDetection(&wbMap, newNode, numNodes)) {
                // Stop at first race detected
                break;
            }
            
            // Race detection with last DMA operation
            // Example: uncached_read after a DMA write has a race with last
            //          DMA operation (write) without a sync in between
            if (hasRace = doRaceDetection(&lastDmaOpMap, newNode, numNodes)) {
                // Stop at first race detected
                break;
            }
        }
        else if (op.find("uncached_write") != string::npos)
        {
            // The node for this uncached write done by CPU 
            // is already added with newNode

            // Stores whether node is visited or not for DFS
            vector<bool> visited(numNodes);

            // Race detection with dangling writebacks
            // Example: uncached_write after a cached write has a race with WB
            //          without a flush in between
            if (hasRace = doRaceDetection(&wbMap, newNode, numNodes)) {
                // Stop at first race detected
                break;
            }
            
            // Race detection with last DMA operation
            // Example: uncached_write after a DMA read/write has a race with 
            //          last DMA operation (write) without a sync in between
            if (hasRace = doRaceDetection(&lastDmaOpMap, newNode, numNodes)) {
                // Stop at first race detected
                break;
            }
        }
        else if (op.find("cached_read") != string::npos) 
        {
            // Add cached read (cr) node
            newCrNode = new Node(CACHE, ++numNodes, "cr" + to_string(count), range);
            tracker.emplace_back(newCrNode);
            DEBUG_STDOUT("Adding cr" + to_string(count));
            newNode->neighbours.push_back(newCrNode);
           
            // Add alloc node for the cr node.
            // If access is aligned (start addr is a multiple of cacheLineSize), generate
            // only one alloc node.
            // If access is unaligned, pre-split into 2 alloc nodes, where the first alloc
            // node touches one cache line and the second alloc node touches another cache
            // line.
            Range cacheBloatedRange1;
            Range cacheBloatedRange2;
            if (iStartAddr % cacheLineSize)
            {
                cacheBloatedRange1 = Range(getLowBloatedAddress(iStartAddr, cacheLineSize), 
                                           getHighBloatedAddress(iStartAddr + 1, cacheLineSize, true));
                newAllocNode1 = new Node(CACHE, ++numNodes, "alloc" + to_string(count), cacheBloatedRange1);
                tracker.emplace_back(newAllocNode1);
                DEBUG_STDOUT("Adding alloc" + to_string(count));
                // alloc points to cr
                newAllocNode1->neighbours.push_back(newCrNode);

                if (cacheBloatedRange1.high() < iEndAddr)
                {
                    // The second alloc node's addr range starts right after cacheBloatedRange1, which
                    // means that it should be aligned if we do add 1.
                    // getHighBloatedAddress(..., true) means that this is the second node of the
                    // split node.
                    cacheBloatedRange2 = Range(cacheBloatedRange1.high() + 1, 
                                               getHighBloatedAddress(iEndAddr, cacheLineSize, true, true));
                    newAllocNode2 = new Node(CACHE, ++numNodes, "alloc" + to_string(count), cacheBloatedRange2);
                    tracker.emplace_back(newAllocNode2);
                    DEBUG_STDOUT("Adding alloc" + to_string(count));
                    newAllocNode2->neighbours.push_back(newCrNode);
                }
            }
            else 
            {
                cacheBloatedRange1 = Range(getLowBloatedAddress(iStartAddr, cacheLineSize), 
                                           getHighBloatedAddress(iEndAddr, cacheLineSize)); 
                newAllocNode1 = new Node(CACHE, ++numNodes, "alloc" + to_string(count), cacheBloatedRange1);
                tracker.emplace_back(newAllocNode1);
                DEBUG_STDOUT("Adding alloc" + to_string(count));
                // cw points to wb node
                newAllocNode1->neighbours.push_back(newCrNode);
            }
         
            auto res1 = allocMap.emplace(cacheBloatedRange1, newAllocNode1);
            auto res2 = res1;   // TODO: how to initialize with correct type
                                // i.e., std::pair<std::_Rb_tree_iterator<std::pair<const Range, Node*> >, bool>
            res2.second = true; // init to true so that it wouldn't connect to a 
                                // non-existent previous alloc node if newAllocNode2 
                                // doesn't exist.
            if (newAllocNode2)
            {
                res2 = allocMap.emplace(cacheBloatedRange2, newAllocNode2);
            }

            // If this cached_read is the first cached operation after a flush,
            // draw an arrow from cr CACHE node (newCrNode) to newAllocNode
            if (isFirstCachedOpAfterFlush && lastFlushNode)
            {
                if (newAllocNode1->addrRange.overlaps(lastFlushNode->addrRange))
                    newCrNode->neighbours.push_back(newAllocNode1);
                
                if (newAllocNode2 && 
                    newAllocNode2->addrRange.overlaps(lastFlushNode->addrRange))
                    newCrNode->neighbours.push_back(newAllocNode2);
                // Draw an arrow from this cr node to a later floating alloc,
                // only if the alloc node's addr range doesn't interfere/overlap
                // with the cr node's addr. (Otherwise, if they overlap, draw an
                // arrow from the cr node's alloc node to a later alloc).
                firstCachedOpAfterFlushNode = newCrNode;
            }
           
            // Update graph
            updateGraph(&graph, newNode, newNode->neighbours);
            updateGraph(&graph, newAllocNode1, newAllocNode1->neighbours);
            if (newAllocNode2)
                updateGraph(&graph, newAllocNode2, newAllocNode2->neighbours);
            // Connect to next CPU operation
            connectToCpuNode = newCrNode;
            needToConnectToCpu = true;

            // DEBUG PRINT ALLOCMAP
            /*
            DEBUG_STDOUT("Printing allocMap...");
            for (auto mit = allocMap.begin(); mit != allocMap.end(); ++mit) 
            {
                DEBUG_STDOUT(intToHex(mit->first.low()) + "-" + intToHex(mit->first.high()) + " latest alloc " + mit->second->name);
            }
            */

            /*
            cout << "res1.second: " << res1.second 
                << "...range: " << intToHex(range.low()) << " - " << intToHex(range.high()) 
                << " vs res1.first->first: " << intToHex(res1.first->first.low()) << " - " 
                                             << intToHex(res1.first->first.high()) << endl; 
            cout << "res2.second: " << res2.second 
                << "...range: " << intToHex(range.low()) << " - " << intToHex(range.high()) 
                << " vs res2.first->first: " << intToHex(res2.first->first.low()) << " - " 
                                             << intToHex(res2.first->first.high()) << endl; 
            */

            map<Range, Node*>::iterator it;
            Node* alloc1WbParent = NULL;
            Node* alloc2WbParent = NULL;
            for (it = wbMap.begin(); it != wbMap.end(); ++it) 
            {
                // Iterate through pairs <addrRange, Node*>
                if (newAllocNode1->addrRange.overlaps(it->first)) 
                {
                    // it->second is the node of the last mem op of addr
                    alloc1WbParent = it->second;
                    DEBUG_STDOUT("alloc1WbParent: " + alloc1WbParent->name);
                    // Do not break yet because newAllocNode2 can have an overlap too
                }
                if (newAllocNode2)
                {
                    if (newAllocNode2->addrRange.overlaps(it->first))
                    {
                        alloc2WbParent = it->second;
                        DEBUG_STDOUT("alloc2WbParent: " + alloc2WbParent->name);
                    }
                }
            }
            // If there is a previous relevant wb node, connect that to
            // this newly created alloc node.
            if (alloc1WbParent || alloc2WbParent)
            {
                // Do not delete the entry from wbMap because we need to propagate
                // any dangling writebacks
                //wbMap.erase(addr);

                if (alloc1WbParent)
                {
                    alloc1WbParent->neighbours.push_back(newAllocNode1);
                    DEBUG_STDOUT("Last mem op is a wb: " + alloc1WbParent->name);
                    updateGraph(&graph, alloc1WbParent, alloc1WbParent->neighbours);
                }
                if (alloc2WbParent)
                {
                    // alloc2WbParent exists only if newAllocNode2 exists, so we don't 
                    // need an extra check to see if newAllocNode2 exists.
                    alloc2WbParent->neighbours.push_back(newAllocNode2);
                    DEBUG_STDOUT("Last mem op is a wb: " + alloc2WbParent->name);
                    updateGraph(&graph, alloc2WbParent, alloc2WbParent->neighbours);
                }

                // Propagate any previous dangling writebacks for this addr on cr node
                newWbNode1 = new Node(CACHE, ++numNodes, "wb" + to_string(count), 
                                      alloc1WbParent->addrRange);
                tracker.emplace_back(newWbNode1);
                DEBUG_STDOUT("Adding wb" + to_string(count));
                newCrNode->neighbours.push_back(newWbNode1);
                if (alloc2WbParent)
                {
                    newWbNode2 = new Node(CACHE, ++numNodes, "wb" + to_string(count),
                                          alloc2WbParent->addrRange);
                    tracker.emplace_back(newWbNode2);
                    DEBUG_STDOUT("Adding wb" + to_string(count));
                    newCrNode->neighbours.push_back(newWbNode2);
                }
                updateGraph(&graph, newCrNode, newCrNode->neighbours);

                // Update wbMap. The latest wb for this address range is this wb
                // (or wb nodes if have split nodes).
                for (it = wbMap.begin(); it != wbMap.end(); ++it)
                {
                    if (it->second == alloc1WbParent)
                    {
                        it->second = newWbNode1;
                    }
                    if (alloc2WbParent)
                    {
                        if (it->second == alloc2WbParent)
                            it->second = newWbNode2;
                    }
                }

                // Update allocMap
                res1.first->second = newAllocNode1;
                if (newAllocNode2)
                    res2.first->second = newAllocNode2;
            }
            // If there is a previous relevant alloc node and this newly created alloc
            // does NOT overlap with a flush, connect that previous alloc to this 
            // newly created alloc node.
            // res is a pair consisting of an iterator to the inserted element (or the
            // already-existing element if no insertion happened), and a bool denoting 
            // whether the insertion took place (true for insertion, false for no insertion).
            // TODO: if res2 is uninitialized, what does res2.second return?? Or should I use
            // res2 == allocMap.end() check instead??
            else if ((!res1.second && range.overlaps(res1.first->first)) ||
                     (!res2.second && range.overlaps(res2.first->first)))
            {
                if (!res1.second)
                {
                    if (isFirstCachedOpAfterFlush && lastFlushNode &&
                        newAllocNode1->addrRange.overlaps(lastFlushNode->addrRange))
                    {
                        // Do NOT draw an arrow from last relevant alloc that is before
                        // the flush if this alloc overlaps with the flush
                    }
                    else
                    {
                        DEBUG_STDOUT("Overlap: " + newAllocNode1->name
                                + "_" + intToHex(newAllocNode1->addrRange.low())
                                + "_" + intToHex(newAllocNode1->addrRange.high()));
                        // res.first is an iterator to pair <addrRange, ptr_to_latest_alloc_node>
                        res1.first->second->neighbours.push_back(newAllocNode1);
                        DEBUG_STDOUT("Prev alloc: " + res1.first->second->name 
                                + "_" + intToHex(res1.first->second->addrRange.low())
                                + "_" + intToHex(res1.first->second->addrRange.high()));
                        updateGraph(&graph, res1.first->second, res1.first->second->neighbours);
                        
                        DEBUG_STDOUT("res1.first->second: " + res1.first->second->name 
                            + " being updated to be " + newAllocNode1->name);
                    }
                    // The latest alloc node for this addr is now this first alloc node
                    res1.first->second = newAllocNode1;
                }

                if (!res2.second)
                {
                    if (isFirstCachedOpAfterFlush && lastFlushNode &&
                        newAllocNode2->addrRange.overlaps(lastFlushNode->addrRange))
                    {
                        // Do NOT draw an arrow from last relevant alloc that is before
                        // the flush if this alloc overlaps with the flush
                    }
                    else
                    {
                        DEBUG_STDOUT("Overlap: " + newAllocNode2->name
                                + "_" + intToHex(newAllocNode2->addrRange.low())
                                + "_" + intToHex(newAllocNode2->addrRange.high()));
                        // res.first is an iterator to pair <addrRange, ptr_to_latest_alloc_node>
                        res2.first->second->neighbours.push_back(newAllocNode2);
                        DEBUG_STDOUT("Prev alloc: " + res2.first->second->name
                                + "_" + intToHex(res2.first->second->addrRange.low())
                                + "_" + intToHex(res2.first->second->addrRange.high()));
                        updateGraph(&graph, res2.first->second, res2.first->second->neighbours);
                        
                        DEBUG_STDOUT("res2.first->second: " + res2.first->second->name 
                            + " being updated to be " + newAllocNode2->name);
                    }
                    // The latest alloc node for this addr is now this second alloc node
                    res2.first->second = newAllocNode2;
                }
            }
            // If there has been a flush, connect the first cached op (cr node) after the flush
            // to this newly created alloc node
            else if (firstCachedOpAfterFlushNode && !isFirstCachedOpAfterFlush)
            {
                firstCachedOpAfterFlushNode->neighbours.push_back(newAllocNode1);
                if (newAllocNode2)
                    firstCachedOpAfterFlushNode->neighbours.push_back(newAllocNode2);
                updateGraph(&graph, firstCachedOpAfterFlushNode, firstCachedOpAfterFlushNode->neighbours);
            }
            isFirstCachedOpAfterFlush = false;
            lastFlushNode = NULL;

            // Note: this checks for races for an alloc node even if this alloc is connected
            // to a previous relevant alloc. TODO
            // Race detection
            if (hasRace = doRaceDetection(&lastDmaOpMap, newAllocNode1, numNodes)) {
                // Stop at first race detected
                break;
            }
            if (newAllocNode2)
            {
                if (hasRace = doRaceDetection(&lastDmaOpMap, newAllocNode2, numNodes)) {
                    // Stop at first race detected
                    break;
                }
            }
        }
        else if (op.find("cached_write") != string::npos)
        {
            // Add cached write (cw) node
            newCwNode = new Node(CACHE, ++numNodes, "cw" + to_string(count), range);
            tracker.emplace_back(newCwNode);
            DEBUG_STDOUT("Adding cw" + to_string(count));
            newNode->neighbours.push_back(newCwNode);
            updateGraph(&graph, newNode, newNode->neighbours);
            // Connect to next CPU operation
            connectToCpuNode = newCwNode; 
            needToConnectToCpu = true;

            // Add writeback (wb) node for the cw node
            // If the access is aligned (start addr is a multiple of wbGranularity), 
            // then generate only one wb node. 
            // If the access is unaligned, pre-split the wb into 2 wb nodes, where the
            // first wb node touches one cache line and the second wb node touches the 
            // next cache line. 
            Range wbBloatedRange1;
            Range wbBloatedRange2;
            if (iStartAddr % wbGranularity)
            {
                wbBloatedRange1 = Range(getLowBloatedAddress(iStartAddr, wbGranularity), 
                                        getHighBloatedAddress(iStartAddr + 1, wbGranularity, true));
                newWbNode1 = new Node(CACHE, ++numNodes, "wb" + to_string(count), wbBloatedRange1);
                tracker.emplace_back(newWbNode1);
                DEBUG_STDOUT("Adding wb" + to_string(count));
                // cw points to wb node
                newCwNode->neighbours.push_back(newWbNode1);

                if (wbBloatedRange1.high() < iEndAddr)
                {
                    // The second wb node's addr range starts right after wbBloatedRange1, which
                    // means that it should be aligned if we do add 1.
                    // getHighBloatedAddress(..., true) means that it's the second node of the
                    // split nodes.
                    wbBloatedRange2 = Range(wbBloatedRange1.high() + 1, 
                                            getHighBloatedAddress(iEndAddr, wbGranularity, true, true));
                    newWbNode2 = new Node(CACHE, ++numNodes, "wb" + to_string(count), wbBloatedRange2);
                    tracker.emplace_back(newWbNode2);
                    DEBUG_STDOUT("Adding wb" + to_string(count));
                    newCwNode->neighbours.push_back(newWbNode2);
                }
            }
            else 
            {
                wbBloatedRange1 = Range(getLowBloatedAddress(iStartAddr, wbGranularity), 
                                        getHighBloatedAddress(iEndAddr, wbGranularity)); 
                //Range wbBloatedRange = Range(iStartAddr-wbGranularity, iEndAddr+wbGranularity); 
                newWbNode1 = new Node(CACHE, ++numNodes, "wb" + to_string(count), wbBloatedRange1);
                tracker.emplace_back(newWbNode1);
                DEBUG_STDOUT("Adding wb" + to_string(count));
                // cw points to wb node
                newCwNode->neighbours.push_back(newWbNode1);
            }
            
            updateGraph(&graph, newCwNode, newCwNode->neighbours);

            // If a write is the first cached op after flush, reset the flag
            // because then, any later cached_reads do not need an arrow to their alloc
            if (isFirstCachedOpAfterFlush)
                isFirstCachedOpAfterFlush = false;

            //// Race detection 
            //if (hasRace = doRaceDetection(&lastDmaOpMap, newWbNode1, numNodes)) {
            //    // Stop at first race detected
            //    break;
            //    // Sync erases entry for addr in lastDmaOpMap, so we cannot
            //    // print that no race was detected here, though the helper function,
            //    // doRaceDetection, still would
            //}
            //// Do race detection for newWbNode2 only if there was a pre-split earlier
            //if (newWbNode2)
            //{
            //    if (hasRace = doRaceDetection(&lastDmaOpMap, newWbNode2, numNodes)) {
            //        // Stop at first race detected
            //        break;
            //    }
            //}
                        
            auto res = wbMap.emplace(wbBloatedRange1, newWbNode1);
            // If there is a previous relevant wb node, connect that to
            // this newly created wb node
            if (!res.second)
            {
                // res.first is an iterator to pair <addr, pointer_to_latest_wb_node>
                res.first->second->neighbours.push_back(newWbNode1);
                DEBUG_STDOUT("Prev wb: " + res.first->second->name);
                updateGraph(&graph, res.first->second, res.first->second->neighbours);

                // The latest wb node for this range is now this wb node
                res.first->second = newWbNode1;
            }
            // Same for second split wb node if it exists
            if (newWbNode2)
            {
                res = wbMap.emplace(wbBloatedRange2, newWbNode2);
                if (!res.second)
                {
                    // res.first is an iterator to pair <addr, pointer_to_latest_wb_node>
                    res.first->second->neighbours.push_back(newWbNode2);
                    DEBUG_STDOUT("Prev wb: " + res.first->second->name);
                    updateGraph(&graph, res.first->second, res.first->second->neighbours);

                    // The latest wb node for this range is now this wb node
                    res.first->second = newWbNode2;
                }
            }

            // Race detection 
            if (hasRace = doRaceDetection(&lastDmaOpMap, newWbNode1, numNodes)) {
                // Stop at first race detected
                break;
                // Sync erases entry for addr in lastDmaOpMap, so we cannot
                // print that no race was detected here, though the helper function,
                // doRaceDetection, still would
            }
            // Do race detection for newWbNode2 only if there was a pre-split earlier
            if (newWbNode2)
            {
                if (hasRace = doRaceDetection(&lastDmaOpMap, newWbNode2, numNodes)) {
                    // Stop at first race detected
                    break;
                }
            }
        }
        else if (op.find("do_dma_read") != string::npos) 
        {
            // Add DMA read
            newDmarNode = new Node(ACC, ++numNodes, "dma_r" + to_string(count), range);
            tracker.emplace_back(newDmarNode);
            DEBUG_STDOUT("Adding dma_r" + to_string(count));
            newNode->neighbours.push_back(newDmarNode);
            updateGraph(&graph, newNode, newNode->neighbours);
            // Do NOT connect DMA operations to next CPU op because
            // they're done in parallel by the accelerator
            needToConnectToCpu = false;

            updateDmaMapsAndGraph(&dmaMap, &lastDmaOpMap, range, newDmarNode, &graph);
            
            // Race detection
            // Check if this DMA read conflicts with another op, i.e. is unordered
            // with respect to a write to same (shared mem) address
            if (hasRace = doRaceDetection(&wbMap, newDmarNode, numNodes)) {
                // Stop at first race detected
                break;
            }
        }
        else if (op.find("do_dma_write") != string::npos) 
        {
            // Add DMA write
            newDmawNode = new Node(ACC, ++numNodes, "dma_w" + to_string(count), range);
            tracker.emplace_back(newDmawNode);
            DEBUG_STDOUT("Adding dma_w" + to_string(count));
            newNode->neighbours.push_back(newDmawNode);
            updateGraph(&graph, newNode, newNode->neighbours);
            // Do NOT connect DMA operations to next CPU op because
            // they're done in parallel by the accelerator
            needToConnectToCpu = false;
        
            updateDmaMapsAndGraph(&dmaMap, &lastDmaOpMap, range, newDmawNode, &graph);
            
            // Race detection
            // Check if this DMA read conflicts with another op, i.e. is unordered
            // with respect to a write to same (shared mem) address
            if (hasRace = doRaceDetection(&wbMap, newDmawNode, numNodes)) {
                // Stop at first race detected
                break;
            }
        }
        else if (op.find("sync") != string::npos)
        {
            // Sync node is already added with newNode.
            // Draw arrow from last DMA op to sync node.
            // TODO: assume that last DMA op is immediately before this sync
            // node, so range is that DMA op's range.
            auto it = lastDmaOpMap.find(range);
            if (it != lastDmaOpMap.end())
            {
                auto neighbours = it->second->neighbours;
                auto resIt = std::find(neighbours.begin(), neighbours.end(), newNode); 
                // Only add the sync node if it is not already a neighbour of 
                // the last DMA op 
                if (resIt == neighbours.end())
                {
                    // it->second is the node of the last DMA op of addr
                    it->second->neighbours.push_back(newNode);
                    DEBUG_STDOUT("Last DMA op is " + it->second->name);
                    updateGraph(&graph, it->second, it->second->neighbours);
                }
                // Sync means no more pending DMA operations
                // TODO: lastDmaOpMap.erase(addr);
                for (auto a = dmaRangesToSync.begin(); a != dmaRangesToSync.end(); ++a)
                {
                    lastDmaOpMap.erase(*a);
                }
                dmaRangesToSync.clear();
            }
        }
        else if (op.find("flush") != string::npos)
        {
            // Flush node is already added with newNode.
            // Bloat flush's address range to be aligned with cacheLineSize, 
            // if necessary.
            newNode->addrRange.setLow(getLowBloatedAddress(iStartAddr, cacheLineSize));
            newNode->addrRange.setHigh(getHighBloatedAddress(iEndAddr, cacheLineSize));
            
            // Draw arrow from dangling wb nodes with relevant address
            // before this flush to this flush node.
            for (auto it = wbMap.begin(); it != wbMap.end(); ) 
            {
                // Iterate through pairs <addrRange, Node*>
                if (newNode->addrRange.overlaps(it->first)) 
                {
                    // wb node's addr range is flushed
                    it->second->neighbours.push_back(newNode);
                    updateGraph(&graph, it->second, it->second->neighbours);
                    
                    // Forget only the writebacks that overlap with this flush
                    wbMap.erase(it++);
                }
                else 
                {
                    ++it;
                }
            }

            // If a cached_read is the next memory operation after a flush,
            // draw an arrow from cached_read to its own alloc and any other
            // alloc after it no matter the address
            isFirstCachedOpAfterFlush = true;
            delete firstCachedOpAfterFlushNode;
            firstCachedOpAfterFlushNode = NULL;

            // Do not draw an arrow from split alloc node to cr if that alloc
            // node's address range does not overlap with that of this flush
            lastFlushNode = newNode;
        }
       
        // Increment count for label to be able to display graph using xdot
        count++;
    } // end while

    inputFile.close();

    if (!hasRace)
    {
        cout << "No race detected" << endl;
    }

#ifdef DEBUG
    // Open output dot file in write mode
    ofstream outputFile;
    outputFile.open("cppoutput.dot");

    DEBUG_STDOUT("Counted " + to_string(numNodes) + " nodes");
    DEBUG_STDOUT("Writing to output file...");
    outputFile << "digraph MyGraph {" << endl;

    map<Node*, vector<Node*> >::iterator it;
    vector<Node*>::iterator nit;
    string first = "";
    string second = "";
    for (it = graph.begin(); it != graph.end(); ++it)
    {
        if (it->first->index >= 167565 || it->first->index <= 167570) {
        first = it->first->name + "_" + intToHex(it->first->addrRange.low()) 
            + "_" + intToHex(it->first->addrRange.high()); 
        // Fill nodes with a specific colour
        if (it->first->nodeType == CPU)
            outputFile << first << " [style=filled, color=green]" << endl;
        else if (it->first->nodeType == CACHE)
            outputFile << first << " [style=filled, color=\".7 .3 1.0\"]" << endl;
        else if (it->first->nodeType == ACC)
            outputFile << first << " [style=filled, color=red]" << endl;

        for (nit = it->second.begin(); nit != it->second.end(); ++nit) 
        {
            second = (*nit)->name + "_" + intToHex((*nit)->addrRange.low()) 
                + "_" + intToHex((*nit)->addrRange.high()); 
            if ((*nit)->nodeType == CPU)
                outputFile << second << " [style=filled, color=green]" << endl;
            else if ((*nit)->nodeType == CACHE)
                outputFile << second << " [style=filled, color=\".7 .3 1.0\"]" << endl;
            else if ((*nit)->nodeType == ACC)
                outputFile << second << " [style=filled, color=red]" << endl;

            outputFile << first << " -> " << second << endl;
        }
        } // end index filter
    }
    outputFile << "}";
    outputFile.close();
    cout << "Completed writing to cppoutput.dot" << endl;
#endif

    // Free memory
    /*
    vector<Node*>::iterator freeit;
    for (it = graph.begin(); it != graph.end(); ++it)
    {
        delete it->first;
        for (freeit = it->second.begin(); freeit != it->second.end(); ++freeit)
        {
            delete *nit;
        }
    }
    */
    for (int i = 0; i < tracker.size(); i++) 
    {
        delete tracker[i];
    }

    return 0;
}
