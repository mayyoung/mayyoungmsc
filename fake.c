#include <stdio.h>
#include <stdint.h>

int8_t read_mic()
{
    return 1;
}

void vector_copy_in(int8_t* a, int8_t* scratch, int size)
{
    int8_t i;
    int8_t j = 30;
    for (i = 0; i < size; i++)
    {
        scratch[i] = a[i];
        printf("do_dma_read %p-%p\n", &a[i], (&a[i]) + sizeof(int8_t) - 1);
    }
}

void vector_fft(int8_t* scratch, int size)
{
    int8_t i;
    for (i = 0; i < size; i++)
    {
        scratch[i] += 2;
    }
}

void vector_copy_out(int8_t* scratch, int8_t* a, int size)
{
    int8_t i;
    for (i = 0; i < size; i++)
    {
        a[i] = scratch[i];
    	printf("do_dma_write %p-%p\n", &a[i], (&a[i]) + sizeof(int8_t) - 1);
    }
}

void flush(int8_t* a) //, int size)
{
    //printf("cache_flusha %p\n", a);
}

void sync()
{
    //printf("sync\n");
}

void print_array(int8_t* a, int size)
{
    int8_t i;
    for (i = 0; i < size; i++)
    {
        printf("a[%d]: %d\n", i, a[i]);
    	printf("cached_read %p-%p\n", &a[i], (&a[i]) + sizeof(int8_t) - 1);
    }
}

int8_t main(void)
{
    int8_t i;
    int N = 10;
    int8_t a[N];
    int8_t scratch1[N]; // this is in accelerator

    for (i = 0; i < N; i++)
    {
        a[i] = read_mic();
        printf("cached_write %p-%p\n", &a[i], (&a[i]) + sizeof(int8_t) - 1);
    }

    flush(a);
    printf("cache_flusha %p-%p\n", &a[0], &a[N-1]);
    
    vector_copy_in(a, scratch1, N);
    vector_fft(scratch1, N);
    // ...
    vector_copy_out(scratch1, a, N);

    sync();
    printf("sync\n");

    //print_array(a, N);

    printf("cached_read %p-%p\n", &a[0], (&a[0]) + sizeof(int8_t) - 1);
    return a[0]; // cached_read
}
