# Last updated: July 9, 2019

# Ref: https://stackabuse.com/read-a-file-line-by-line-in-python/
import sys
import os
import re

def main():
    filepath = sys.argv[1]

    if not os.path.isfile(filepath):
        print("Filepath {} does not exist. Exiting...".format(filepath))
        sys.exit()

    cpuList = []        # cpu instructions parsed from program
    cacheList = []      # cache instructions parsed from program
    accList = []        # accelerator instructions parsed from program
    count = 0           # count is needed to distinguish nodes in dot
    with open(filepath, "r") as fp:
        for line in fp:
            # if line.startswith("//"):
            #   continue # skip comments
            if "load" in line:
                cpuList.append("ld%d" % count)
                cacheList.append({"name": "cr%d" % count,
                        "address": "0x1234"}) # dummy address
            elif "store" in line:
                cpuList.append("st%d" % count)
                cacheList.append({"name": "cw%d" % count,
                        "address": "0x1234"}) # assume only one address for now
            #elif "cache_flusha" in line:
            #    cpuList.append("flusha%d" % count)
            #elif "cache_flushb" in line:
            #    cpuList.append("flushb%d" % count)
            elif "vbx_dma_to_vector" in line:
                cpuList.append("cpu_acc_r%d" % count)
                accList.append("dma_r%d" % count)
            elif "vbx_dma_to_host" in line:
                cpuList.append("cpu_acc_w%d" % count)
                accList.append("dma_w%d" % count)
            elif "sync" in line:
                cpuList.append("sync%d" % count)
            count += 1

    #for i in range(0, len(cacheList)):
    #    print("cacheList[%d]: %s" % (i, cacheList[i]["name"]))

    with open("output.dot", "w") as out:
        out.write("digraph MyGraph {\n")
        
        # Fill nodes with a specific colour
        for i in range(0, len(cpuList)):
            out.write(cpuList[i] + " [style=filled, color=green]\n")
        for i in range(0, len(cacheList)):
            out.write(cacheList[i]["name"] + " [style=filled, color=\".7 .3 1.0\"]\n")
        for i in range(0, len(accList)):
            out.write(accList[i] + " [style=filled, color=red]\n")

        # Connect all CPU operations in program order
        for i in range(0, len(cpuList)):
            if i > 0:
                out.write(cpuList[i-1] + " -> " + cpuList[i] + "\n")

            # 'sync' is synchronous blocking, returns after all DMA operations
            # have completed.
            # Due to transitivity, we only need to connect the latest relevant
            # DMA operation to this sync node.
            if "sync" in cpuList[i]:
                out.write(accList[len(accList) - 1] + " -> " + cpuList[i] + "\n") 
            
            # 'flusha' probes cache for a specific address and evicts/writebacks
            # if present. It only evicts a cache block if tag matches.
            # if "flusha" in cpuList[i]:
                # TODO: draw an arrow from each dangling writeback with the 
                # relevant address to this flusha node. Any alloc (of correct address) 
                # after this flush needs an arrow to it from this flusha node 
                # if this flush is the latest flush.

            # 'flushb' probes cache for a specific block address and evicts/writebacks
            # if present. It always evicts a cache block (no tag comparisons).
            # if "flushb" in cpuList[i]:
                # TODO: draw an arrow from each dangling writeback with the 
                # relevant address to this flushb node. Any alloc (of correct address) 
                # after this flush needs an arrow to it from this flushb node
                # if this flush is the latest flush node.

        # Cache operations (read/write)
        for i in range(0, len(cacheList)):
            name = cacheList[i]["name"]
            numsList = re.findall(r'\d+', name)
            currNum = numsList[0]
            address = cacheList[i]["address"]

            if i > 0:
                out.write(cacheList[i-1]["name"] + " -> " + name + "\n")

            # Cached reads by CPU
            if "cr" in name:
                # Connect from corresponding CPU instruction to this cache instruction
                out.write(("ld%s -> " + name + "\n") % currNum)
                
                if int(currNum) > 0:
                    # If previous node is a read, connect current alloc node 
                    # with previous one
                    if "cr" in cacheList[i-1]["name"]:
                        out.write("alloc{0} [style=filled, color=\".7 .3 1.0\"]\n".format(int(currNum)-1))
                        out.write("alloc%d -> alloc%s\n" % ((int(currNum)-1), currNum))
                    
                    # If previous node is a write, that means there's a writeback node.
                    # Then the read needs "alloc" to read the value from memory into 
                    # the cache.
                    elif "cw" in cacheList[i-1]["name"]:
                        out.write("wb%d -> alloc%s\n" % ((int(currNum)-1), currNum))
                
                # Every read of any value from cache needs an alloc node
                out.write("alloc{0} [style=filled, color=\".7 .3 1.0\"]\n".format(currNum))
                out.write("alloc" + currNum + " -> " + name + "\n")
                
                # The read connects back to CPU subgraph.
                # The control passes back to the CPU because it needs that read value.
                index = cpuList.index("ld%s" % currNum)
                out.write(name + " -> " + cpuList[index + 1] + "\n")

            # Cached writes by CPU
            elif "cw" in name:
                # Connect from corresponding CPU instruction to this cache instruction
                out.write(("st%s -> " + name + "\n") % currNum)
                
                # If write is first instruction, need to allocate from memory
                # into cache. 
                # NOT doing this, to make it more consistent, alloc only for reads
                # if i == 0:
                #    out.write("alloc" + currNum + " -> " + name + "\n")
                
                # Otherwise, we don't need an alloc.
                # Every cached write needs a writeback node.
                out.write("wb{0} [style=filled, color=\".7 .3 1.0\"]\n".format(currNum))
                out.write(name + " -> wb" + currNum + "\n")
                
                # If a cached write is previous instruction, then connect the
                # writeback nodes.
                if int(currNum) > 0 and "cw" in cacheList[i-1]["name"]:
                    out.write("wb%d -> wb%s\n" % ((int(currNum)-1), currNum))
                
                # The write connects back to CPU subgraph.
                # This arrow exists assuming the semantics that it's an acknowledgement
                # that the write instruction was received by the cache.
                # Note: Could not draw this arrow if the semantics had meant the CPU
                #       is waiting for the write to finish before proceeding.
                index = cpuList.index("st%s" % currNum)
                out.write(name + " -> " + cpuList[index + 1] + "\n")

        # DMA operations (read/write) by accelerator
        for i in range(0, len(accList)):
            #print("i: {0}, accList item: {1}\n".format(i, accList[i]))
            numsList = re.findall(r'\d+', accList[i])
            currNum = numsList[0]

            # Connect DMA operations together (accelerator program order).
            # Note: we do not need to do these if DMA operations were run in parallel
            # or out of order.
            if i > 0:
                out.write(accList[i-1] + " -> " + accList[i] + "\n")

            # DMA reads
            if "dma_r" in accList[i]:
                index = cpuList.index("cpu_acc_r%s" % currNum)

            # DMA writes
            elif "dma_w" in accList[i]:
                index = cpuList.index("cpu_acc_w%s" % currNum)
            
            # Connect corresponding CPU instruction to accelerator instruction
            out.write(cpuList[index] + " -> " + accList[i] + "\n")
            
            # Note: We do not connect back to CPU subgraph because the CPU does not
            # wait for the DMA operation to finish (that's the purpose of 'sync').

        out.write("}")



if __name__ == '__main__':
    main()
